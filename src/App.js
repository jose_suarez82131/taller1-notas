import React from 'react';
import Main from './components/Main'
import './App.css';
import 'bulma/css/bulma.css'

function App() {
  return (
    <div className="App">
      <Main/>
    </div>
  );
}

export default App;
