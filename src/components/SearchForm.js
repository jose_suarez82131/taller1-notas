import React, { Component } from 'react'


export default class SearchForm extends Component {

    state = {
        filter: 'Todos',
        nombre: ''
    }

    setFilter = (e) => {
        this.setState({filter: e.target.value})
    }

    setName = (e) => {
        this.setState({nombre: e.target.value})
    }

    sendFilter = (e) => {
        e.preventDefault()
        const {filter, nombre} = this.state
        this.props.onResults(filter, nombre)
    }

    render() {

        return (
            <div>
                <form id="frmSearch"  onSubmit={this.sendFilter}>
                    <div className="control" name="options">
                        <label className="radio">
                            <input type="radio" name="answer" value="Ganó" onChange={this.setFilter}/>
                            Ganaron
                        </label>
                        
                        <label className="radio">
                            <input type="radio" name="answer" value="Perdió" onChange={this.setFilter}/>
                            Perdieron
                        </label>
                        
                        <label className="radio">
                            <input type="radio" name="answer" value="Todos" onChange={this.setFilter}/>
                            Todos
                        </label>
                        
                    </div>
                    <div className="field">
                        <p className="control has-icons-left has-icons-right">
                            <input className="input" type="text" placeholder="Nombre"/>
                            <span className="icon is-small is-left">
                            <i className="fa fa-search" aria-hidden="true"></i>
                            </span>
                        </p>
                        <button className="button">Filtrar</button>
                    </div>
                </form>
            </div>
        )
    }
}
