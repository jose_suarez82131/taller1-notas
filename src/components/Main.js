import React, { Component } from 'react'
import Title from './Title'
import CaptureForm from './CaptureForm'
import SearchForm from './SearchForm'
import Table from './Table'
import Footer from './Footer'

export default class Main extends Component {

    state = {
        students: [],
        filter: String,
        nombre: String
    }

    setFilter = (filter, nombre) => {
        this.setState({nombre: nombre})
        this.setState({filter: filter})
    }

    addStudent = (student) => {
        let located = false
        // eslint-disable-next-line eqeqeq
        if(student.codigo != "" && student.parcial != "" && student.final != "" && student.seguimiento != "") {
            this.state.students.forEach(st => {
                if(st.codigo === student.codigo) {
                    alert("El alumno con código " + student.codigo + ' ya fue registrado')
                    located = true
                }
            });
            if(!located) {
                let students2
                students2 = this.state.students
                students2.push(student)
                this.setState({students: students2})
                this.setFilter('Todos', '')
            }  
        } else {
            alert("Debe completar todos los campos")
        }   
    }

    render() {
        return (
            <div>
                <Title title="Taller 1 React Basic 2020" subtitle="Registro de Notas"></Title>
                <div className="columns">
                    <div className="column">
                        <CaptureForm onResults={this.addStudent}/>
                    </div>
                    <div className="column">
                        <SearchForm students={this.state.students} onResults={this.setFilter}/>
                        {
                            <Table students={this.state.students} filter={this.state.filter} nombre={this.state.nombre}/>
                        }
                    </div>
                </div>
                <Footer></Footer>
            </div>
        )
    }
}
