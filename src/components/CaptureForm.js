import React, { Component } from 'react'

export default class CaptureForm extends Component {

    state = {
        codigo: '',
        nombre : '',
        parcial: 0,
        final: 0,
        seguimiento: 0,
        definitiva: 0,
        estado: ''
    }

    setCode = (e) => {
        this.setState({codigo: e.target.value}) 
    }

    setStudent = (e) => {
        this.setState({nombre: e.target.value})
    }
        
    setParcial = (e) => {
        this.setState({parcial: e.target.value})
    }

    setFinal = (e) => {
        this.setState({final: e.target.value})
    }

    setFollow= (e) => {
        this.setState({seguimiento: e.target.value})
    }

    setDefinitive= (e) => {
        this.setState({definitiva: e.target.value})
    }

    sendStudent = (e) => {
        e.preventDefault()
        const student = this.state
        student.definitiva = student.parcial * 0.25 + student.final * 0.25 + student.seguimiento * 0.5;
        if(student.definitiva < 3) {
            student.estado = 'Perdió'
        } else {
            student.estado = 'Ganó'
        }
        this.props.onResults(student)
        document.getElementById("frmStudent").reset();
        document.getElementById("definitiveInput").value = 0
        document.getElementById("inputCodigo").focus()
    }




    render() {
        return (
            <div>
                <form id ="frmStudent" className="frmCapture" onSubmit={this.sendStudent}>
                    <div>
                        <input type="text" placeholder="Codigo" className="input" onChange = {this.setCode} id="inputCodigo"></input>

                    </div>
                    <div>
                        <input type="text" placeholder="Nombre del Alumno" className="input" onChange = {this.setStudent}></input>
                    </div>
                    <div>
                        <input type="number" placeholder="Nota Parcial" className="input" min = "0" max = "5" step="any" onChange = {this.setParcial}></input>
                    </div>
                    <div>
                        <input type="number" placeholder="Nota Final" className="input" min = "0" max = "5" step="any" onChange = {this.setFinal}></input>  
                    </div>
                    <div>
                        <input type="number" placeholder="Nota Seguimiento" className="input" min = "0" max = "5" step="any" onChange = {this.setFollow}></input>
                    </div> 
                    <hr></hr>
                    <div>
                        <label>Definitiva</label>
                        <input type="text" className="input" id="definitiveInput" disabled value={this.state.parcial * 0.25 + this.state.final * 0.25 + this.state.seguimiento * 0.5}></input>
                    </div>
                    <button className="button is-small" type="submit">Guardar</button>                   
                </form>
                
            </div>
        )
    }
}
