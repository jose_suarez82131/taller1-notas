import React from 'react'

export default function Footer() {

    
    return (
        <footer>
            <h5 className="subtitle"><b>Presentado por:</b> José Andrés Suárez Espinal</h5>
            <h6 className="subtitle">C.C. 71.387.452</h6>
        </footer>
    )
}
