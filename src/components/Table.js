import React, { Component } from 'react'
import PropTypes from 'prop-types'


export default class Table extends Component {

    static propTypes = {
        students: PropTypes.array,
        filter: PropTypes.string,
        nombre: PropTypes.string
    }
    render() {
        const {students, filter, nombre} = this.props
        console.log(nombre)
        return (
            <div>
                    <table className="table">
                        <thead className="thead">
                            <tr>
                                <th>Alumno</th>
                                <th>Nota</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                students.map((st, index) => {
                                    if(filter === 'Todos' && st.nombre.includes(nombre)) {
                                        return(
                                            <tr key={index}>
                                                <td>{st.nombre}</td>
                                                <td>{st.definitiva}</td>
                                                <td>{st.estado}</td>
                                            </tr>
                                        )
                                    } else {
                                        if(st.estado === filter && st.nombre.includes(nombre)) {
                                            return(
                                                <tr key={index}>
                                                    <td>{st.nombre}</td>
                                                    <td>{st.definitiva}</td>
                                                    <td>{st.estado}</td>
                                                </tr>
                                            )
                                        }
                                    }
                                })
                            }
                        </tbody>
                    </table>
            </div>
        )
    }
}
